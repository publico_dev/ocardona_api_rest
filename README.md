---

## Proyecto Omar Cardona - ApiRest para AlMundo.com

Este proyecto es un ApiRest desarrollado para la prueba de almundo.com. Tiene como objetivo poder realizar el CRUD completo para una aplicación de hoteles.

Está desarrollado con node V6.11.3 y tiene como dependencias:
1. body-parser: 1.18.2: Para la el manejo de request.
2. express: 4.16.2: Proporciona métodos de utilidad HTTP para la estructura del ApiRest.
3. mongodb: 2.2.19: Persistencia de datos.

## Puesta en marcha

El proyecto está diseñado para funcionar con una base de datos proporcionada por mlab.com que se encuentra en línea, de tal forma que la puesta en marcha de la aplicación es muy rapida:

1. Descargar el repositorio: git clone https://oacardona@bitbucket.org/publico_dev/ocardona_api_rest.git
2. Ingresar al directorio del proyecto descargado
3. Ejecutamos: npm install
4. Ejecutamos: npm start

Si todo salió bien, ya deberia estar en consola un mensaje que nos indica que el servidor está arriba:

*Escuchando en http://127.0.0.1:8000*

Si por algun motivo no existen datos en la BD, solo debemos usar el API de configuración

## ApiRest

Esta son los servicios que presta:

1. Descripcion: Carga de hoteles a la BD.
2. Tipo: POST
3. EndPoint: /setup
4. Parametros: action=uploadDb
---
1. Descripcion: Consulta de todos los hoteles.
2. Tipo: GET
3. EndPoint: /hotels
4. Parametros: 
---
1. Descripcion: Consulta de un hotel por id.
2. Tipo: GET
3. EndPoint: /hotels/:id
4. Parametros: id
---
1. Descripcion: Consulta de hoteles filtrado por nombre y estrellas.
2. Tipo: GET
3. EndPoint: /hotels/filter/
4. Parametros: name, stars (Los campos son opcionales)
---
1. Descripcion: Creación de un hotel.
2. Tipo: POST
3. EndPoint: /hotels
4. Parametros: id, name, stars, price, image, amenities[]
---
1. Descripcion: Actualización de datos de un hotel.
2. Tipo: PUT
3. EndPoint: /hotels/:id
4. Parametros: id, name, stars, price, image, amenities[] (Los campos son opcionales)
---
1. Descripcion: Eliminación de un hotel por id.
2. Tipo: DEL
3. EndPoint: /hotels/:id
4. Parametros: id
