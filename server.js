const express        = require('express');
const MongoClient    = require('mongodb').MongoClient;
const bodyParser     = require('body-parser');
const app            = express();
const db             = require('./config/db');

const port = 8000;

app.use(bodyParser.urlencoded({ extended: true })); // Habilitar la Captura de URL Encoded

var options = { server: { reconnectTries: 2000,reconnectInterval: 1000 }} 

MongoClient.connect(db.url, options, (err, database) => {
  	if (err) return console.log(err)
  
  	require('./app/routes')(app, database);
  
  	app.listen(port, () => {
    	console.log('Escuchando en http://127.0.0.1:' + port);
  	});

});


