


module.exports = function(app, db) {
	// Objeto de respuesta
	var objResp = null;

	// Funcion que permite iniciar el objeto de respuesta
	function clearObjResp(){
		objResp = {
			'status': 'true',
			'message': '',
			'data': null
		}
	}

	// GET - LIST
	app.get('/hotels', (req, res) => {
		clearObjResp();
    	db.collection('hotels').find().toArray(function(err, items) {
        	if (err) {
        		oobjResp.status = false;
				objResp.message = 'ERROR: ' + err;
			} else {
			   	objResp.message = 'get-all';
			   	objResp.data = items;
			}
			res.send( objResp );
        });

  	});

  	// GET - list - Filters
  	app.get('/hotels/filter', (req, res) => {
  		clearObjResp();
  		var filterElement = {};

  		if(req.query.id)
  			filterElement.id = req.query.id
  		if(req.query.name)
  			filterElement.name = {"$regex": req.query.name, "$options": "i"}
  		if(req.query.stars)
  			filterElement.stars = parseInt(req.query.stars)

  		if( Object.keys(filterElement).length === 0 ) {
  			objResp.status = false;
			objResp.message = 'No existen filtros';
			objResp.data = new Array();
			res.send( objResp );
		}

    	db.collection('hotels').find(filterElement).toArray(function(err, item) {
			if (err) {
				oobjResp.status = false;
				objResp.message = 'ERROR: ' + err;
			} else {
			   	objResp.message = 'get-all-by-filter';
			   	objResp.data = item;
			}
			res.send( objResp );
	    });
  	});

	// GET - ONE - byId
  	app.get('/hotels/:id', (req, res) => {
  		clearObjResp();
    	const id = req.params.id;
    	const filterElement = { 'id': id };

    	db.collection('hotels').findOne(filterElement, (err, item) => {
			if (err) {
				oobjResp.status = false;
				objResp.message = 'ERROR: ' + err;
			} else {
			   	objResp.message = 'get-one';
			   	objResp.data = new Array(item);
			}
			res.send( objResp );
	    });
  	});

  	// POST
	app.post('/hotels', (req, res) => {
		clearObjResp();
		const hotel = { 
			id: req.body.id, 
			name: req.body.name, 
			stars: req.body.stars,
			price: req.body.price,
			image: req.body.image,
			amenities: req.body.amenities
		};

		db.collection('hotels').insert(hotel, (err, result) => {
			if (err) { 
				oobjResp.status = false;
				objResp.message = 'ERROR: ' + err;
			} else {
			   	objResp.message = 'post';
			   	objResp.data = result.ops[0];
			}
			res.send( objResp );
		});
			
	});

	// PUT
	app.put('/hotels/:id', (req, res) => {
		clearObjResp();
		const filterElement = { 'id': req.params.id };
		var hotel = { 
			id: req.params.id, 
			name: req.body.name, 
			stars: req.body.stars,
			price: req.body.price,
			image: req.body.image,
			amenities: req.body.amenities
		};

		// Busqueda del item que se va a actualizar
		db.collection('hotels').findOne(filterElement, (err, item) => {
			if (err) {
				oobjResp.status = false;
				objResp.message = 'ERROR: ' + err;
				res.send( objResp );
			} else {
				if(item){
					hotelOld = item;

					if(!req.body.name)
						hotel.name = item.name

					if(!req.body.stars)
						hotel.stars = item.stars

					if(!req.body.price)
						hotel.price = item.price

					if(!req.body.image)
						hotel.image = item.image

					if(!req.body.amenities)
						hotel.amenities = item.amenities


					db.collection('hotels').update(filterElement, hotel, (err, result) => {
						if (err) {
						  	oobjResp.status = false;
							objResp.message = 'ERROR: ' + err;
						} else {
						   	objResp.message = 'put';
						   	objResp.data = hotel;
						}
						res.send( objResp );
					});
				} else {
					objResp.status = false;
					objResp.message = 'No existe el item';
					res.send( objResp );
				}
			} 
	    });
	});

	// DELETE
	app.delete('/hotels/:id', (req, res) => {
		clearObjResp();
		const id = req.params.id;
		const filterElement = { 'id': id };
		db.collection('hotels').remove(filterElement, (err, item) => {
			if (err) {
				oobjResp.status = false;
				objResp.message = 'ERROR: ' + err;
			} else {
			   	objResp.message = 'delete';
			   	objResp.data = item;
			}
			res.send( objResp );
		});
	});


};