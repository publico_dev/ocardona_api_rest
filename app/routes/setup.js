
module.exports = function(app, db) {
	// Objeto de respuesta
	var objResp = null;

	// Funcion que permite iniciar el objeto de respuesta
	function clearObjResp(){
		objResp = {
			'status': 'true',
			'message': '',
			'data': null
		}
	}

	app.post('/setup', (req, res) => {
		clearObjResp();
		const action = req.body.action.toUpperCase();

		// Carga de Dato (Hoteles) a la BD
		if( action == "UPLOADDB") {
			var dataJson = require('../../data/data.json');

			try{
		    
			   	db.collection('hotels').insertMany( dataJson, (err, item) => {
					if (err) {
						objResp.status = false;
						objResp.message = 'ERROR: ' + err;
					} else {
					   	objResp.message = 'Carga de datos exitosa ';
					   	objResp.data = item;
					}
					res.send( objResp );
			    });
			   	
			} catch (e) {
			   	objResp.status = false;
				objResp.message = 'ERROR: ' + e;
				res.send( objResp );
			}
		} 
		// DEFAULT
		else {
			objResp.status = false;
			objResp.message = 'Opción no válida	';
			res.send( objResp );
		}

	});
};