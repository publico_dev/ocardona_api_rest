const setupRoutes = require('./setup');
const hotelRoute = require('./hotel');

module.exports = function(app, db) {
  setupRoutes(app, db);
  hotelRoute(app, db);
};